{
  KozDbBridge
  -----------

  Author: Kozarin L. A.
  Email: kozalo@yandex.ru
  Website: http://kozalo.ru
  Repository: https://bitbucket.org/Kozalo/kozdbbridge


  This unit is supposed to be a middleware between different database components and your program.
  It provides a unified interface for you to use in your code.
  Out of the box, the bridge has implementations for ODAC and dbExpress (formerly known as BDE).


  ============================
  How to work with this module
  ============================

  To make a long story short, we have 2 interfaces: IDbConnection and IDbIterableResult.
  The first one is used to establish a connection to the database and execute your queries.
  Standard TOraConnection and TBdeConnection classes have constructors which take 3 parameters: connection string, username, and password.
  To insert, delete or update rows use its method Execute(SQL: string).
  To select some information from the database use the Select(SQL: string) method. It returns an implementation of the IDbIterableResult interface.
  The second interface is used as an iterator between columns and rows.
  It provides 3 methods for you to use. All of them are called without any arguments. They are:
  - MoreColsLeft - returns true if there is more columns to iterate;
  - NextCol - returns a string with the content of the next column (you can cast it to any type you need);
  - NextRow - use this one in while loop to reach the next row; returns false, if there is no rows left.

  Also there are one event (TOnLastRowEvent) and two exceptions (EDbConnectionError and EDbNoMoreColsError).
  Standard implementations of IDbIterableResult have a public field OnLastRow: TOnLastRowEvent.
  By default, it's used to close the query when we reached the last row in selection, but you can assign your own listener (don't forget to close the query by yourself!).
  EDbConnectionError is raised if we couldn't establish the connection. It has a message to help you find the error.
  EDbNoMoreColsError is raised if we tried to iterate to the next column, but there is no more columns left.
  EDbExecuteQueryError is raised if an error occurred while processing the query.


  ==============
  Simple example
  ==============

  function TestFunc: TStringList;
  var
    db: IDbConnection;
    selection: IDbIterableResult;
    someList: TStringList;
  begin
    someList := TStringList.Create;

    db := TBdeConnection.Create(Self, 'localhost:1521/ora12c', 'admin', 'admin');
    selection := db.Select('SELECT first_name, last_name FROM Employees');

    while selection.NextRow do begin
      while selection.MoreColsLeft do begin
        someList.Add(selection.NextCol);
      end;
    end;

    Result := someList;
    FreeAndNil(selection);
    FreeAndNil(db);
  end;


  ---
  Be a kawaii neko, colleague! ^_^
  Nyan~! :3

  Nov, 2016
}


unit KozDbBridge;

//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
interface
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////

uses
  SysUtils, Dialogs, Classes, DB, DBAccess, Ora, AdoDB, SqlExpr;

type

////////////////////////////////////////
///  INTERFACES, EVENTS AND EXCEPTIONS
////////////////////////////////////////

  // Raised when we have problems while connecting to the database.
  EDbConnectionError = class(Exception);

  // Raised when we had reached the last column of a row and tried to iterate again.
  EDbNoMoreColsError = class(Exception)
    constructor Create;
  end;

  // Raised if an error occurred while processing the query.
  EDbExecuteQueryError = class(Exception);

  // This event occures when we reached the last row in selection.
  // Usually it's used for closing the query, but you can assign your own listener (don't forget to close the query in this case!)
  TOnLastRowEvent = procedure() of object;

  IDbIterableResult = interface
    function MoreColsLeft: Boolean;   // Returns true if there is more columns to iterate.
    function NextCol: String;         // Returns a string with the content of the next column (you can cast it to any type you need).
    function NextRow: Boolean;        // Use this one in while loop to reach the next row; returns false, if there is no rows left.
  end;

  IDbConnection = interface
    function Select(query: string): IDbIterableResult;  // Use this method to select any information from the database.
    procedure Execute(query: string);                   // Use this one to execute insert, update, delete (and so on) queries.
    procedure CloseQuery;     // You need to use it manually if you're not going to reach the last row in selection or you reassigned the listener for TOnLastRowEvent.
                              // Also the query will be closed automatically before performing the next one.
  end;


///////////////////////////////
///  ODAC
///////////////////////////////

  TOraIterableResult = class(TInterfacedObject, IDbIterableResult)
    constructor Create;
    destructor Destroy;
  public
    OnLastRow: TOnLastRowEvent;
    function MoreColsLeft: Boolean;
    function NextCol: String;
    function NextRow: Boolean;
  private
    FieldCounter: Integer;
    DbQuery: TOraQuery;
  end;

  TOraConnection = class(TInterfacedObject, IDbConnection)
    constructor Create(AOwner: TComponent; server, username, password: string);
    destructor Destroy;
  public
    function Select(query: string): IDbIterableResult;
    procedure Execute(query: string);
    procedure CloseQuery;
  private
    DbConnection: TOraSession;
    DbQuery: TOraQuery;
    QueryClosed: Boolean;
  end;


///////////////////////////////
///  dbExpress
///////////////////////////////

  TBdeIterableResult = class(TInterfacedObject, IDbIterableResult)
    constructor Create;
    destructor Destroy;
  public
    OnLastRow: TOnLastRowEvent;
    function MoreColsLeft: Boolean;
    function NextCol: String;
    function NextRow: Boolean;
  private
    FieldCounter: Integer;
    DbQuery: TSqlQuery;
    FirstRow: Boolean;
  end;

  TBdeConnection = class(TInterfacedObject, IDbConnection)
    constructor Create(AOwner: TComponent; server, username, password: string);
    destructor Destroy;
  public
    function Select(query: string): IDbIterableResult;
    procedure Execute(query: string);
    procedure CloseQuery;
  private
    DbConnection: TSqlConnection;
    DbQuery: TSqlQuery;
    QueryClosed: Boolean;
  end;


//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
implementation
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////

constructor EDbNoMoreColsError.Create;
begin
  inherited Create('No more fields left.');
end;


///////////////////////////////
///  ODAC
///////////////////////////////

/// TOraIterableResult ///

constructor TOraIterableResult.Create;
begin
  inherited;
  FieldCounter := 0;
end;

destructor TOraIterableResult.Destroy;
begin
  DbQuery.Close;
  FreeAndNil(DbQuery);
end;

function TOraIterableResult.MoreColsLeft;
begin
  if FieldCounter < DbQuery.Fields.Count then
    Result := true
  else
    Result := false;
end;

function TOraIterableResult.NextCol: String;
begin
  if not MoreColsLeft then begin
    raise EDbNoMoreColsError.Create;
    Exit;
  end;

  Result := DbQuery.Fields[FieldCounter].AsString;
  Inc(FieldCounter);
end;

function TOraIterableResult.NextRow: Boolean;
begin
  if not DbQuery.Eof then begin
    // I don't know exactly why it works in such way, but on my test Oracle database I had to call the Next() method even for the first row to get the right response.
    DbQuery.Next;
    FieldCounter := 0;
    Result := true;
  end else begin
    Result := false;

    if Assigned(OnLastRow) then
      OnLastRow;
  end;
end;


/// TOraConnection ///

constructor TOraConnection.Create(AOwner: TComponent; server, username, password: string);
begin
  inherited Create;
  QueryClosed := true;

  DbConnection := TOraSession.Create(AOwner);
  DbConnection.Options.Direct := true;
  DbConnection.Server := server;
  DbConnection.Username := username;
  DbConnection.Password := password;

  try
    DbConnection.Connect;
  except
    on E: Exception do begin
      raise EDbConnectionError.Create(E.Message);
      Self.Destroy;
    end;
  end;

  DbQuery := TOraQuery.Create(AOwner);
  DbQuery.Session := DbConnection;
end;

destructor TOraConnection.Destroy;
begin
  DbQuery.Close;
  FreeAndNil(DbQuery);

  DbConnection.Disconnect;
  FreeAndNil(DbConnection);
end;

function TOraConnection.Select(query: string): IDbIterableResult;
var
  DbResult: TOraIterableResult;
begin
  if not QueryClosed then
    CloseQuery;

  DbQuery.SQL.Text := query;

  try
    DbQuery.Open;
  except
    on E: Exception do begin
      raise EDbExecuteQueryError.Create(Format('An error occurred while processing the following query: %s. Response from the database: %s.', [query, E.Message]));
      Exit;
    end;
  end;
  QueryClosed := false;

  DbResult := TOraIterableResult.Create;
  DbResult.OnLastRow := CloseQuery;
  DbResult.DbQuery := DbQuery;

  Result := DbResult;
end;

procedure TOraConnection.Execute(query: string);
begin
  DbQuery.SQL.Text := query;

  try
    DbQuery.Execute;
  except
    on E: Exception do begin
      raise EDbExecuteQueryError.Create(Format('An error occurred while processing the following query: %s. Response from the database: %s.', [query, E.Message]));
      Exit;
    end;
  end;
end;

procedure TOraConnection.CloseQuery;
begin
  DbQuery.Close;
  QueryClosed := true;
end;


///////////////////////////////
///  dbExpress
///////////////////////////////

/// TBdeIterableResult ///

constructor TBdeIterableResult.Create;
begin
  inherited;
  FieldCounter := 0;
  FirstRow := true;
end;

destructor TBdeIterableResult.Destroy;
begin
  DbQuery.Close;
  FreeAndNil(DbQuery);
end;

function TBdeIterableResult.MoreColsLeft;
begin
  if FieldCounter < DbQuery.Fields.Count then
    Result := true
  else
    Result := false;
end;

function TBdeIterableResult.NextCol: String;
begin
  if not MoreColsLeft then begin
    raise EDbNoMoreColsError.Create;
    Exit;
  end;

  Result := DbQuery.Fields[FieldCounter].AsString;
  Inc(FieldCounter);
end;

function TBdeIterableResult.NextRow: Boolean;
begin
  // In dbExpress the way how the Eof propery works is kind of unusual.
  // To not get an empty row at the end, we have to call the Next() method first.
  if not FirstRow then
    DbQuery.Next
  else
    FirstRow := false;

  if not DbQuery.Eof then begin
    FieldCounter := 0;
    Result := true;
  end else begin
    Result := false;

    if Assigned(OnLastRow) then
      OnLastRow;
  end;
end;


/// TBdeConnection ///

constructor TBdeConnection.Create(AOwner: TComponent; server, username, password: string);
begin
  inherited Create;
  QueryClosed := true;

  DbConnection := TSqlConnection.Create(AOwner);
  DbConnection.DriverName := 'Oracle';
  with DbConnection.Params do begin
    Add('DataBase=' + server);
    Add('User_name=' + username);
    Add('Password=' + password);
  end;

  try
    DbConnection.Connected := true;
  except
    on E: Exception do begin
      raise EDbConnectionError.Create(E.Message);
      Self.Destroy;
    end;
  end;

  DbQuery := TSQLQuery.Create(AOwner);
  DbQuery.SQLConnection := DbConnection;
end;

destructor TBdeConnection.Destroy;
begin
  DbQuery.Close;
  FreeAndNil(DbQuery);

  DbConnection.Close;
  FreeAndNil(DbConnection);
end;

function TBdeConnection.Select(query: string): IDbIterableResult;
var
  BdeResult: TBdeIterableResult;
begin
  if not QueryClosed then
    CloseQuery;

  DbQuery.SQL.Text := query;
  DbQuery.Open;
  QueryClosed := false;

  BdeResult := TBdeIterableResult.Create;
  BdeResult.OnLastRow := CloseQuery;
  BdeResult.DbQuery := DbQuery;

  Result := BdeResult;
end;

procedure TBdeConnection.Execute(query: string);
begin
  DbQuery.SQL.Text := query;
  DbQuery.ExecSQL;
end;

procedure TBdeConnection.CloseQuery;
begin
  DbQuery.Close;
  QueryClosed := true;
end;

end.
