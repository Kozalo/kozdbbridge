# KozDbBridge #

This unit is supposed to be a middleware between different database components and your program.  
It provides a unified interface for you to use in your code.  
Out of the box, the bridge has implementations for ODAC and dbExpress (formerly known as BDE).  

Модуль, предоставляющий единый интерфейс для работы с БД, используя реализацию различных компонентов.  
На данный момент имеются реализации для ODAC и dbExpress (бывшем BDE).  

### Documentation / Документация ###

* [In English](https://bitbucket.org/Kozalo/kozdbbridge/wiki/Documentation%20in%20English)  
* [На русском языке](https://bitbucket.org/Kozalo/kozdbbridge/wiki/%D0%94%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D0%B0%D1%86%D0%B8%D1%8F%20%D0%BD%D0%B0%20%D1%80%D1%83%D1%81%D1%81%D0%BA%D0%BE%D0%BC%20%D1%8F%D0%B7%D1%8B%D0%BA%D0%B5)